<?php

namespace Naolis\Bundle\ConnectorBundle\Reader\Product;

use Akeneo\Bundle\BatchBundle\Entity\StepExecution;
use Akeneo\Bundle\BatchBundle\Item\AbstractConfigurableStepElement;
use Doctrine\ORM\EntityManager;
use Naolis\Bundle\ConnectorBundle\Converter\Product\OptionsTranslationConverter;
use Pim\Bundle\BaseConnectorBundle\Reader\ProductReaderInterface;
use Pim\Bundle\BaseConnectorBundle\Validator\Constraints\Channel as ChannelConstraint;
use Pim\Bundle\CatalogBundle\Entity\Category;
use Pim\Bundle\CatalogBundle\Entity\Channel;
use Pim\Bundle\CatalogBundle\Entity\Repository\CategoryRepository;
use Pim\Bundle\CatalogBundle\Manager\ChannelManager;
use Pim\Bundle\CatalogBundle\Repository\ProductCategoryRepositoryInterface;
use Pim\Bundle\CatalogBundle\Repository\ProductRepositoryInterface;
use Pim\Bundle\TransformBundle\Converter\MetricConverter;
use Symfony\Component\Validator\Constraints as Assert;

class CategoryTreeReader extends AbstractConfigurableStepElement implements ProductReaderInterface
{
    const LIMIT = 100;

    /**
     * @var ChannelManager
     */
    protected $channelManager;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var ProductCategoryRepositoryInterface
     */
    protected $productCategoryRepository;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var MetricConverter
     */
    protected $metricConverter;

    /**
     * @var OptionsTranslationConverter
     */
    protected $optionsTranslationConverter;

    /**
     * @var string|Channel
     *
     * @Assert\NotBlank(groups={"Execution"})
     * @ChannelConstraint
     */
    protected $channel;

    /**
     * @var StepExecution
     */
    protected $stepExecution;

    /**
     * @var integer
     */
    protected $offset = 0;

    /**
     * @var null|integer[]
     */
    protected $ids = null;

    /**
     * @var \ArrayIterator
     */
    protected $products;

    /**
     * @var Category[]
     */
    protected $categories;

    /**
     * @var Category
     */
    protected $currentCategory;

    /**
     * @param ChannelManager                     $channelManager
     * @param CategoryRepository                 $categoryRepository
     * @param ProductCategoryRepositoryInterface $productCategoryRepository
     * @param ProductRepositoryInterface         $productRepository
     * @param EntityManager                      $entityManager
     * @param MetricConverter                    $metricConverter
     * @param OptionsTranslationConverter        $optionsTranslationConverter
     */
    public function __construct(
        ChannelManager $channelManager,
        CategoryRepository $categoryRepository,
        ProductCategoryRepositoryInterface $productCategoryRepository,
        ProductRepositoryInterface $productRepository,
        EntityManager $entityManager,
        MetricConverter $metricConverter,
        OptionsTranslationConverter $optionsTranslationConverter
    ) {
        $this->channelManager = $channelManager;
        $this->categoryRepository = $categoryRepository;
        $this->productCategoryRepository = $productCategoryRepository;
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
        $this->metricConverter = $metricConverter;
        $this->optionsTranslationConverter = $optionsTranslationConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigurationFields()
    {
        return [
            'channel' => [
                'type'    => 'choice',
                'options' => [
                    'choices'  => $this->channelManager->getChannelChoices(),
                    'required' => true,
                    'select2'  => true,
                    'label'    => 'pim_base_connector.export.channel.label',
                    'help'     => 'pim_base_connector.export.channel.help'
                ]
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function read()
    {
        $product = null;

        if (!$this->products->valid()) {
            $this->products = $this->getNextProducts();

            while (null === $this->products && !empty($this->categories)) {
                $this->currentCategory = array_shift($this->categories);
                $this->stepExecution->getExecutionContext()->put('category', $this->currentCategory);
                $this->ids = null;
                $this->offset = 0;
                $this->products = $this->getNextProducts();
            }
        }

        if (null !== $this->products) {
            $product = $this->products->current();
            $this->products->next();
            $this->stepExecution->incrementSummaryInfo('read');
        }

        if (null !== $product) {
            $this->metricConverter->convert($product, $this->channel);
            $this->optionsTranslationConverter->convert($product, $this->channel);
        }

        return $product;
    }

    /**
     * {@inheritdoc}
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    /**
     * {@inheritdoc}
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * {@inheritdoc}
     */
    public function setStepExecution(StepExecution $stepExecution)
    {
        $this->stepExecution = $stepExecution;
    }

    /**
     * {@inheritdoc}
     */
    public function initialize()
    {
        $this->entityManager->getConnection()->getConfiguration()->setSQLLogger(null);

        $this->products = new \ArrayIterator();
        $this->ids = null;
        $this->offset = 0;

        if (!is_object($this->channel)) {
            $this->channel = $this->channelManager->getChannelByCode($this->channel);
        }

        if (null === $this->categories) {
            $qb = $this->categoryRepository->getAllChildrenQueryBuilder($this->channel->getCategory(), true);
            $qb->innerJoin($qb->getRootAliases()[0].'.translations', 'translations')
                ->addSelect('translations');
            $this->categories = $qb->getQuery()->execute();

            if (!empty($this->categories)) {
                $this->currentCategory = array_shift($this->categories);
                $this->stepExecution->getExecutionContext()->put('category', $this->currentCategory);
            }
        }
    }

    /**
     * Get next products page.
     *  If category product ids is not initialized, initialize it
     *  Slice these ids to fetch products collection and return it
     *
     * @return \ArrayIterator
     */
    protected function getNextProducts()
    {
        $this->entityManager->clear();
        $products = null;

        if (null === $this->ids) {
            $this->ids = $this->getProductIdsInCategory();
        }

        $currentIds = array_slice($this->ids, $this->offset, static::LIMIT);

        if (!empty($currentIds)) {
            $products = new \ArrayIterator($this->productRepository->findByIds($currentIds));
            $this->offset += static::LIMIT;
        }

        return $products;
    }

    /**
     * Query database for exportable products in the current category
     *
     * @return array
     */
    protected function getProductIdsInCategory()
    {
        $ids = $this->productCategoryRepository->getProductIdsInCategory($this->currentCategory);
        if (count($ids) === 0) {
            return array();
        }

        $query = $this->productRepository
            ->buildByChannelAndCompleteness($this->channel);

        $rootAlias = current($query->getRootAliases());
        $rootIdExpr = sprintf('%s.id', $rootAlias);

        $from = current($query->getDQLPart('from'));

        $query
            ->select($rootIdExpr)
            ->resetDQLPart('from')
            ->from($from->getFrom(), $from->getAlias(), $rootIdExpr)
            ->andWhere($query->expr()->in($rootIdExpr, $ids))
            ->groupBy($rootIdExpr)
        ;

        $results = $query->getQuery()->getArrayResult();

        return array_keys($results);
    }
}
