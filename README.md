Easycatalog Connector by Naolis
========================

## Project Introduction

*Specifical connector to Easycatalog for akeneo.* Easycatalog is a plug-in that enhance database publishing capabilities of Adobe® InDesign®.

Ideal for time-critical publications, EasyCatalog can dramatically speed up page make-up time and ensure your documents remain error free. Trusted by thousands of users in over thirty countries across six continents, EasyCatalog has quickly established itself as one of the most powerful and flexible database publishing solutions for Adobe® InDesign®.

You can find Easycatalog at [this link](http://www.65bit.com/software/easycatalog/)

The connector provides two *jobs*, one allowing an export to a CSV file, the other to a mySQL table named *EZC-'channel-name'* is made to work with the Easycatalog ODBC system.

These two *jobs* have the same behaviors :

- if a product is available in several categories, the product is as many time present in export
- the products are classified following the tree of their categories
- the path of the product's category in the tree structure is present in export on the basis of one column per depth level
- the headers don't have the mention of the language and the channel

## Naolis

Naolis is a software publisher which develops in France solutions to multi channel. We are the Akeneo's technologic partner for Web-to-print solutions. We assist users and eCommerce integrators in the implementation of database publishing from Akeneo.

« Easycatalog Connector by Naolis » for Akeneo is a free Bundle, available for download.

This Bundle will be gradually improved by the adding of new features, so please feel free to address to us your needs to complete our roadmap.

Naolis, publisher of the solution « Easycatalog Connector by Naolis » can assist you along customization, support and consulting for Akeneo.

Naolis is an Easycatalog expert since 2007, professional solution of database publishing for Adobe Indesign, we operate  project audit, settings and support of Easycatalog as well as the user’s training.

We are able to listen to you for any questions at this mail : techinfo@naolis.com

| Fonction                  | Name                  | Email                                 | 
| ------------------------- | --------------------- | ------------------------------------- | 
| *Technical*     | Aymeric Morilleau     | techinfo@naolis.com                |
| *Sales*     | Jean-Luc Martinez     | contact@naolis.com                |

## Installation

Add the bundle to your installation Akeneo in src/Naolis/Bundle/ConnectorBundle and the next line to AppKernel.php :

    new Naolis\Bundle\ConnectorBundle\NaolisConnectorBundle()


### mySQL configuration

Allow the LOAD applicative side by configurating Doctrine to use PDO::MYSQL_ATTR_LOCAL_INFILE in the option of adding the next lines to 
config.yml of Symfony2 application.

    doctrine:
        dbal:
            default_connection:   default
            connections:
                default:
                    options:
                        1001: true

Give the rights of the file to the client on the mySQL server.

    grant file on *.* to akeneo_pim@localhost identified by 'akeneo_pim';

## Technical notes

These two *jobs* use the same processor. Therefore there are two different processors, one of the export table extending the one of the CSV export to expose less configuration as possible. The processor allow the use of a Sorter service to sort out the lines before to send them back to Writer.

Finally, to have several lines for each item ans several columns for one attribute, a specifical Normalizer
and Encoder have been written which can support the ncsv format (for Naolis CSV).

The *job* table use a specifical writer allowing to import the data of a temporary csv in a mySQL table.
The import using LOAD DATA INFILE of mySQL so it's necessary to authorize the mySQL server to access to the files and the application to use the LOCAL hence the needed configuration for mySQL

## Possible issues

### Error mysql 1148

The error mySQL "1148 The used command is not allowed with the MySQL version" can happen while using the connector to a mySQL server.

It's proper to check firstly if the LOCAL INFILE option is well activated on the SQL server side.
In the my.conf of mySQL we can check that the line is well present.

    local-infile = 1

Under Ubuntu 14,04 LTS, it's possible that the mySQL client package for the php5 installed by default does not allow the option. In that case, install the php5-mysqlnd package which will replace by itself php5-mysql.

    sudo apt-get install php5-mysqlnd

## Licences

The Open Software License version 3.0

Full license is at: http://opensource.org/licenses/OSL-3.0

---

*Developed by [Naolis](http://www.naolis.com)*

